import color_terrm as ct
from DBJson import DBJson
from DBSqlite import DBSqlite
import prettydate
import config
import toXml
import export_html
import sys

def printTodo(self,todo):
    print "id "+ct.colored_text(str(todo.id),ct.GREEN) + " todo " +ct.colored_text( prettydate.pretty_date(todo.created), ct.LBLUE)
    print "{:^40}".format(todo.message)


def getDB():
    if config.STORAGE_TYPE=='json':
        DB=DBJson
    else:
        DB=DBSqlite
    db=DB(config.STORAGE_PATH)
    return db

def command_add(msg):
    db=getDB()
    db.add(msg)
    print "Todo added"

def command_done(id):
    db=getDB()
    db.setDone(id)
    print"Todo setted to done"

def command_list(where,file=None):
    db = getDB()
    if where=='xml':
        toXml.toXml(db.todo_list,file)
    elif where=='html':
        export_html.todos_html_format(file,db.todo_list)
    else:
        for todo in db.todo_list:
            printTodo(todo)

def main():
    cmd=sys.argv[1]
    if cmd=='add':
        message=""
        for x in sys.argv[2:]:
            message+=x
        command_add(message)
    elif cmd=='done':
        command_done(int(sys.argv[2]))
    elif cmd=='list':
        command_list()
    else:
        print "Command "+cmd+" not found"

main()