import prettydate

def todos_html_format(filename,todos):
    header = "<!DOCTYPE html><html><head><title>Todos</title></head><body>"
    footer = "</body></html>"
    with open(filename, "w") as f:
        f.write(header)
        for todo in todos:
            f.write("<div>{0} {1} {2} {3}</div>".format(todo.id,
                                                        todo.message,
                                                        todo.done,
                                                        prettydate.pretty_date(todo.created)))
        f.write(footer)


