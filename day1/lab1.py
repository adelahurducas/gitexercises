# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with =
# print make_title('ana')
# Ana
# ===
# use the string method upper
def make_title(message):
    return message.title()+'\n'+'-'*len(message)
print make_title('adela')
# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]
def invert_tuples(lst):
    out=[]
    for x,y in lst:
        if x==y:
            continue
        else:
            x,y=y,x
            t=x,y
            out.append(t)
    return out

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    i=0

    for x in lst:
        if x in curses.keys():
            lst[i]=curses[x]
        i+=1

    return lst
# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    if len(s)<2:
        return ""
    else:
        return s[:2]+s[-2:]

# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    rez=1
    for i in range(1,n+1):
        rez*=i
    if n<2:
        return 1
    else:
        return reduce(lambda a , b : a * b, range(1,n+1))




def fibo(n):
    list=[]
    list.append(0)
    list.append(1)
    fib1=0
    fib2=1
    while fib1+fib2<n:
        list.append(fib1+fib2)
        aux=fib1
        fib1=fib2
        fib2=fib2+aux
    return list

#----- testing code below ---

def test_make_title():
    assert make_title('ana') == 'Ana\n---'

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker'], {'fucker': 'f**er'}) == ['you', 'f**er']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

