Git exercises
=============

You should only use the command line.



1)  Create an empty git repository in the directory git-exercises

2)  Create 3 files: a.txt b.txt c.txt
    Commit a.txt

    Git status should look like this

    $ git status
    On Branch master
    Untracked files
    b.txt
    c.txt

3) Add all files to the stagin area

    $ git status

    On branch master
    Changes to be committed:
        new file:   b.txt
        new file:   c.txt

4) Remove c.txt from the commit

    $ git status
    On branch master
    Changes to be committed:
        new file:   b.txt

    Untracked files:
        x.txt

5) add *.pyc .idea to .gitignore and commit it

6) Create a fibo.py which prints the fibonacci numbers less than 100 using a recursive implementation
    Commit the file.
    $ python fibo.py
    1 1 2 3 5 ...

7) Create an experiments branch. Add a iterative implementation.

8) Merge the iterative implementation into master

9) On master implement tests that check that the 2 implementations produce the same results

9) continue the experimental branch by reading the upper limit from the command line
    $ python fibo.py 50

10) merge experimental in master. Resolve the conflict

11) create a bitbucket repository. Make it a remote for this local git repository and push the master branch to bitbucket

