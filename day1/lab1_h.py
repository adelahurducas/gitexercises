import math
# Homework problems for unit 1
# ----------------------------

#The rest of dividing a square number like 81, by 32 can only be
#0, 1, 4, 9, 16, 17, 25
#A number that has such a rest when divided by 32 is called a pseudo square mod 32
def pseudo_square_mod32(n):
    rests=[0,1,4,9,16,17,25]
    if n%32 in rests:
        return True
    else:
        return False

def certain_square(n):
    return int(n**0.5 + 0.5)**2 == n

certain_square(81)

# Test how useful is the maybe_square test.
# How many false positives it reports for numbers under 1000?
# What percentage of tests are false positives?
def maybe_square_accuracy():
    N = 10000
    count=0
    for x in range(1,1000):
        if pseudo_square_mod32(x) and not certain_square(x):
            count+=1
    percentage=count*0.1
    return count, percentage

# Given a list of numbers, return a list where
# all adjacent == elements have been reduced to a single element,
# so [1, 2, 2, 3] returns [1, 2, 3]. You may create a new list or
# modify the passed in list.
def remove_adjacent(nums):
    counting_list=[0]*len(nums)
    for x in nums:
        counting_list[x]+=1
    set=[]
    for i in range(0,len(counting_list)):
        if counting_list[i]>0:
            set.append(i)
    return set

# Receives a color intensity as a int.
# 0 is black, 255 is the most saturated red, 128 is a middle red
# It should return the color in a float space where 0.0 is black
# and 1.0 is the most saturated red. 0.5 is that middle red.
# int_color_to_float_color(128) == 0.5
def int_color_to_float_color(red):
    pass

# similar to the above case, but now we want to map black to tm
# and the reddest of reds, the 255 to tM
# int_color_to_float_range(128, 0, 2) == 1.0
def int_color_to_float_range(red, tm, tM):
    pass

# rescale transforms a number from the range fm..fM to the tm..tM range
# This is the generalization of the color use case
# but now f ranges not from 0..255 but from fm..fM
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    f -=fm
    f/=fM-fm
    f*= (tM-tm)
    f+= tm
    return f

def test_int_color_to_float_color():
    assert abs(int_color_to_float_color(51) - 0.2) < 1e-2
    assert abs(int_color_to_float_color(155) - 0.6078) < 1e-2

def test_int_color_to_float_range():
    assert abs(int_color_to_float_range(51, 1, 2) - 1.2) < 1e-2
    assert abs(int_color_to_float_range(128, -2, 2) - 0) < 1e-2

def test_pseudo_square_mod32():
    assert pseudo_square_mod32(9)
    assert not pseudo_square_mod32(24)
    assert pseudo_square_mod32(17)

def test_remove_adjacent():
    assert remove_adjacent([1, 2, 2, 3]) == [1, 2, 3]
    assert remove_adjacent([2, 2, 3, 3, 3]) == [2, 3]
    assert remove_adjacent([]) == []

def test_rescale():
    assert rescale(5, 0, 10, 0, 1) == 0.5
    assert abs(rescale(3, 2, 7, -1, 2) + 0.4) < 1e-4
