
def day_of_week(m,d,y):

    months={'Jan':1, 'Feb':2,'Mar':3, 'Apr':4, 'May':5, 'Jun': 6 , 'Jul':7 , 'Aug':8 , 'Sep': 9, 'Oct':10, 'Nov':11, 'Dec': 12}
    days={0:'Sunday',1:'Monday', 2:'Tuesday', 3:'Wednesday', 4:'Thursday', 5:'Friday', 6:'Saturday'}
    month=months[m]
    y0=y-(14-month)/12
    x=y0+y0/4-y0/100+y0/400
    m0=month+12*((14-month)/12)-2
    d0=(d+x+31*m0/12)%7
    return days[d0]

print day_of_week('Jun',20,2017)