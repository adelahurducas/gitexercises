def toesc(c):
    """
    Returns the ansii escape with the code c
    \x1b[cm
    """
    return '\x1b[' +str(c)+'m'
ENDC=toesc(0)
LBLACK, LRED, LGREEN, LYELLOW, LBLUE, LPURPLE, LCYAN, LWHITE=[toesc(i) for i in range (90,98)]
GREEN=toesc(32)
def colored_text(text, color=ENDC):
    """ returns the text surrounded by ansii escapes that would make it print in the given color on a terminal """
    return color+text+ENDC