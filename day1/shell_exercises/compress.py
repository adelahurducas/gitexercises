def compress(lista):
    result=[]
    i=0
    while i<len(lista)-2:
        count=1

        while  i<=len(lista)-2 and lista[i]==lista[i+1]:
            count+=1
            i+=1
        t=count,lista[i]
        result.append(t)
        i+=1
    return result


def decompress(lista):
    result=[]
    for c, x in lista:
        result=result+[x]*c
    return result

print decompress(compress([0,0,0,1,1,7,0,0,0,5,6,7,7]))

def test_compress():
    assert compress([0,0,0,1,1,7,0,0,0])==[(3, 0), (2, 1), (1, 7), (3, 0)]
    assert  compress([0,0,0,1,1,7,0,0,0,5,6,7,7])==[(3, 0), (2, 1), (1, 7), (3, 0), (1, 5), (1, 6)]
    assert  compress([])==[]

def test_decompress():
    assert decompress(compress([0,0,0,1,1,7,0,0,0]))==[0,0,0,1,1,7,0,0,0]
    assert decompress(compress([0,0,0,1,1,7,0,0,0,5,6,7,7]))==[0,0,0,1,1,7,0,0,0,5,6,7,7]
    assert  decompress(compress([]))==[]

test_compress()
test_decompress()