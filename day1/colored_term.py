import os
import math
"""
Ansii escape codes are magic strings that the terminal interprets as commands
Some of the most ab-used ones change the color of the text
They begin with the character 27 = 0x1b.
Python strings can contain hex escapes so chr(27) == '\x1b'
The syntax of ansii escapes is
\x1b[21m
\x1b[34m
etc. In general \x1b[numberm
"""

def toesc(c):
    """
    Returns the ansii escape with the code c
    \x1b[cm
    """
    return '\x1b[' +str(c)+'m'

# codes from 90 to 98 and from 30 to 38 change the text's color
# code zero 0 resets the terminal to the default state

# define the reset code
ENDC = toesc(0)
# define the following colors corresponding to the code range 90 98
LBLACK, LRED, LGREEN, LYELLOW, LBLUE, LPURPLE, LCYAN, LWHITE=[toesc(i) for i in range (90,98)]
# define the following colors corresponding to the code range 30 38
DBLACK, DRED, DGREEN, DYELLOW, DBLUE, DPURPLE, DCYAN, DWHITE=[toesc(i) for i in range(30,38)]

def colored_text(text, color=ENDC):
    """ returns the text surrounded by ansii escapes that would make it print in the given color on a terminal """
    return color+text+ENDC

def welcome_python():
    """ prints Python welcomes the terminal. Each work in a different color"""
    print colored_text("Python ",DRED)+colored_text("welcomes ",DGREEN)+colored_text("the ",DYELLOW)+colored_text("terminal",DBLUE)+colored_text("!",DPURPLE)

welcome_python()

def nr_items(path):
    files=os.listdir(path)
    return len(files)


def get_size(file):
    length = os.path.getsize(file)
    if length<1024:
        return str(length)+'b'
    elif length >=1024:
        kb=length/1024
        if math.trunc(kb)>1024:
            #verfica sa fie gigabytes:
            mb=kb/1024
            if math.trunc(mb)>1024:
                return str(mb/1024)+'gb'
            else:
                return str(mb)+'mb'

        else:
            return str(kb)+'kb'


def get_size1(file):
    length = os.path.getsize(file)
    dim='bkmG'
    for x in dim:
        if length<1024:
            return str(length)+x
        else:
            length=length/1024

def get_extensie(file):
    if not '.' in file:
        return ""
    else:
        ext=""
        i=0
        ok=0
        while i<len(file):
            if file[i]==".":
                ok=1

            if ok==0:
                i+=1
                continue
            ext+=file[i]
            i+=1
        return ext

def list_files(path):

    path=path
    list=os.listdir(path)
    directors=[]
    files=[]
    print list
    for f in list:
        path_file=path+'/'+f
        if os.path.isdir(path_file):

            directors.append(f)
            #print "{}{:>10}    items".format(colored_text(f,LBLUE),nr)
        else:
            files.append(f)


    sorted_directors=sorted(directors)

    for d in sorted_directors:
        path_file = path + '/' + d
        nr = 0
        nr = nr_items(path_file)
        print '{}{:>10}    '.format(colored_text(d, LBLUE), colored_text(str(nr),LBLUE))+colored_text('items',LBLUE)

    sorted_files=sorted(files)

    for f in files:
        path_file = path + '/' + f
        print colored_text(f, LGREEN) + "   " + get_extensie(f)+'              '+get_size1(path_file)





