# this function should return a tuple
# the first element is a list of integers representing the isbn
# the second element is the check digit
# parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def parse_isbn(isbn):
    isbn_check = int(isbn[-1])
    isbn_main = []
    # todo finish this function
    for i in range(4,len(isbn)-2):
        isbn_main.append(int(isbn[i]))

    return isbn_main, isbn_check



def is_isbn_valid(isbn):
    checksum = 0
    # todo
    isbn_check = int(isbn[-1])
    count=10
    isbn_main = []
    for i in range(4, len(isbn) - 2):
        isbn_main.append(int(isbn[i]))
        checksum+=count*int(isbn[i])
        count-=1
    checksum+=isbn_check*count
    return checksum % 11 == 0

# py.test code below

def test_parse_isbn():
    assert parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def test_is_isbn_valid():
    assert is_isbn_valid('ISBN817525766-0')
    assert not is_isbn_valid('ISBN817525767-0')
