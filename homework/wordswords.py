def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """
    rez=[(word[0:i],word[i:]) for i in xrange(0,len(word)+1)]
    return rez

def deletes(word):
    """
    all misspellings of word caused by omitting a letter
    use the splits function
    #>>> 'rockets' not in deletes('rocket')
    True
    #>>> 'roket' in deletes('rocket')
    True
    """
    result=[word[0:i]+word[i+1:] for i in xrange(0,len(word))]
    return result

def substitutions(word):
    alphabet='abcdefghijklmnopqrstuvwxyz'
    for i in xrange(0,len(word)):
        result=[word[0:i]+l+word[i+1:] for l in alphabet]
    return result
print substitutions('adela')