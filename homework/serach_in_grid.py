def search_in_grid(grid,elem):
    i=0
    j=0
    while i<len(grid)-1  and j<len(grid[i])-1:

        if elem>grid[i+1][j]:
            i+=1
        if elem>grid[i][j+1]:
            j+=1
        if grid[i][j]==elem:
            return True
    return False

print search_in_grid([[1,2,3],[4,5,6],[6,7,8]],5)

