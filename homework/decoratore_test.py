def myfunc(a):
    return {'a is': [a]}


import json

def to_json(func):
    def inner(*arg):
        ret=func(*arg)
        return json.dumps(ret)
    return inner

print myfunc(6)

