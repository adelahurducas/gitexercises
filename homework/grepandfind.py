import os
import re


def grep_in_file(path,pattern):
    with open(path) as file:
        for line in file:
            if re.search(pattern, line):
                print '{} : {}'.format(path, line)

def list_files(path,file_filter,line_filter):

    for root,dir,files in os.walk(path):
        for file in files:
            if re.search(file_filter,file):
                path_file = os.path.join(root,file)
                grep_in_file(path_file,line_filter)



list_files('/home/adela/module1-exercises',r'\.py$','class')
